FROM alpine:latest
MAINTAINER "Geoff Price <geoffrey.a.price@gmail.com>"

# Pass version data to image 
# (Not used in build, for user information only)
COPY ./VERSION.txt /

# Use ARG to prevent needing to update this file
# with every release version
ARG TERRAFORM_VERSION

# Environment variables
ENV TERRAFORM_FILENAME=terraform_${TERRAFORM_VERSION}_linux_amd64.zip
ENV TERRAFORM_URL=https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/${TERRAFORM_FILENAME}
ENV TERRAFORM_SHA256URL=https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS

# Install dependancies and set shell
RUN apk add --update python3 git bash curl wget shadow ruby
RUN usermod --shell /bin/bash root
RUN gem install rake

# Symlink python3 to python for compatability
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN ln -s /usr/bin/pip3 /usr/bin/pip

# Install Terraform
RUN curl -O ${TERRAFORM_URL} 2> /dev/null \
  && echo "`curl ${TERRAFORM_SHA256URL} 2> /dev/null | awk '/linux_amd64/{print $1}'`  ${TERRAFORM_FILENAME}" | sha256sum -c
RUN unzip ${TERRAFORM_FILENAME} -d /bin
RUN rm -f ${TERRAFORM_FILENAME}
